const express = require("express");

const app = express();

app.use('/static', express.static('public'))
app.set('view engine', 'pug')
app.set('views', './src/views')

app.get("/", (request, response) => {
    response.render("index");
})

app.listen(3000, () => {
    console.log("Server are running on 3000 port");
})
